const http  = require('http');
const parse = require('request-form');

var url    = require('url');
var Canvas = require('canvas')
var fs     = require('fs');
var net    = require('net');

const requestHandler = (request, response) => {
    var queryData = url.parse(request.url, true).query;

    var Image     = Canvas.Image;
    var canvas    = new Canvas(800, 400);
    var ctx       = canvas.getContext('2d');

    var w   = (queryData.w   !== undefined) ? queryData.w   : 'WE MAKE';
    var wx  = (queryData.wx  !== undefined) ? queryData.wx  : '400';
    var wy  = (queryData.wy  !== undefined) ? queryData.wy  : '160';
    var ws  = (queryData.ws  !== undefined) ? queryData.ws  : '150';
    var wc  = (queryData.wc  !== undefined) ? queryData.wc  : '#000000';
    var p   = (queryData.p   !== undefined) ? queryData.p   : 'PORN';
    var px  = (queryData.px  !== undefined) ? queryData.px  : '400';
    var py  = (queryData.py  !== undefined) ? queryData.py  : '350';
    var ps  = (queryData.ps  !== undefined) ? queryData.ps  : '220';
    var pc  = (queryData.pc  !== undefined) ? queryData.pc  : '#000000';
    var co  = (queryData.co  !== undefined) ? queryData.co  : '#FCD205';
    var bgt = (queryData.bgt !== undefined) ? queryData.bgt : 'plain';
    var rc  = (queryData.rc  !== undefined) ? queryData.rc  : '#000000';
    var bgr = (queryData.bgr !== undefined) ? queryData.bgr : '#FCD205';
    var egr = (queryData.egr !== undefined) ? queryData.egr : '#FFFFFF';
    var or  = (queryData.or  !== undefined) ? queryData.or  : 'lr';
    var cor = (queryData.cor !== undefined) ? queryData.cor : '20';

    parse(request, {})
    .then(data => {
        if (data.text !== undefined) {
            response.writeHead(200, {
                'Content-Type': 'application/json'
            });
            response.end(JSON.stringify({response_type: 'in_channel', text: fullUrl(request)+'?p='+encodeURIComponent(data.text)}));
        } else {
            switch(bgt) {
                case 'plain':
                    ctx.fillStyle = hexToRgbA(co);
                    break;
                case 'gradient':
                    var grd;
                    switch(or) {
                        case 'lr':
                            grd = ctx.createLinearGradient(0, 200, canvas.width, 200);
                            break;
                        case 'rl':
                            grd = ctx.createLinearGradient(canvas.width, 200, 0, 200);
                            break;
                        case 'tb':
                            grd = ctx.createLinearGradient(400, 0, 400, canvas.height);
                            break;
                        case 'bt':
                            grd = ctx.createLinearGradient(400, canvas.height, 400, 0);
                            break;
                        case 'r':
                            grd = ctx.createRadialGradient(400, 200, 25, 400, 200, 400);
                            break;
                    }
                    grd.addColorStop(0, bgr);
                    grd.addColorStop(1, egr);
                    ctx.fillStyle = grd;
                    break;
                case 'rainbow':
                    var grd = ctx.createLinearGradient(400, 0, 400, canvas.height);
                    grd.addColorStop(0, '#e50000');
                    grd.addColorStop(1 / 5, '#ff8d00');
                    grd.addColorStop(2 / 5, '#ffee00');
                    grd.addColorStop(3 / 5, '#008121');
                    grd.addColorStop(4 / 5, '#004cff');
                    grd.addColorStop(1, '#760188');
                    ctx.fillStyle = grd;
                    break;
            }
            roundRect(ctx, 0, 0, canvas.width, canvas.height, parseInt(cor), true, false);

            // Inside rounded rectangle
            ctx.lineWidth   = 20;
            ctx.strokeStyle = hexToRgbA(rc);
            roundRect(ctx, 25, 25, 750, 350, 20, false);

            ctx.textAlign="center";
            // Write WE MAKE
            ctx.font      = 'bold '+parseInt(ws)+'px sans-serif';
            ctx.fillStyle = hexToRgbA(wc);
            var i = 0;
            while (ctx.measureText(w).width > 725) {
                ctx.font      = 'bold '+(parseInt(ws) - i++)+'px sans-serif';
            }
            ctx.fillText(w, parseInt(wx), parseInt(wy));

            // Write PORN
            ctx.font      = 'bold '+parseInt(ps)+'px sans-serif';
            ctx.fillStyle = hexToRgbA(pc);
            i = 0;
            while (ctx.measureText(p).width > 725) {
                ctx.font      = 'bold '+(parseInt(ps) - i++)+'px sans-serif';
            }
            ctx.fillText(p, parseInt(px), parseInt(py));

            // Get data
            var data = canvas.toDataURL().replace('data:image/png;base64,','');

            // Create image
            var img = new Buffer(data, 'base64');

            // Send image
            response.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': img.length
            });
            response.end(img);
        }
    }, err => {
    });
}

const server = http.createServer(requestHandler)

server.listen('/tmp/wemawema.sock', (err) => {
    if (err) {
        return console.log('something bad happened', err);
    } else {
        console.log('server is listening on socket /tmp/wemawema.sock');
    }
});

server.on('error', function (e) {
    if (e.code == 'EADDRINUSE') {
        var clientSocket = new net.Socket();
        clientSocket.on('error', function(e) { // handle error trying to talk to server
            if (e.code == 'ECONNREFUSED') {  // No other server listening
                fs.unlinkSync('/tmp/wemawema.sock');
                server.listen('/tmp/wemawema.sock', (err) => {
                    if (err) {
                        return console.log('something bad happened', err)
                    }
                });
            }
        });
        clientSocket.connect({path: '/tmp/wemawema.sock'}, function() {
            console.log('Server running, giving up...');
            process.exit();
        });
    }
});

function roundRect(ctx, x, y, width, height, radius, fill, stroke) {
    if (typeof stroke == 'undefined') {
        stroke = true;
    }
    if (typeof radius === 'undefined') {
        radius = 5;
    }
    if (typeof radius === 'number') {
        radius = {tl: radius, tr: radius, br: radius, bl: radius};
    } else {
        var defaultRadius = {tl: 0, tr: 0, br: 0, bl: 0};
        for (var side in defaultRadius) {
            radius[side] = radius[side] || defaultRadius[side];
        }
    }
    ctx.beginPath();
    ctx.moveTo(x + radius.tl, y);
    ctx.lineTo(x + width - radius.tr, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr);
    ctx.lineTo(x + width, y + height - radius.br);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height);
    ctx.lineTo(x + radius.bl, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl);
    ctx.lineTo(x, y + radius.tl);
    ctx.quadraticCurveTo(x, y, x + radius.tl, y);
    ctx.closePath();
    if (fill) {
        ctx.fill();
    }
    if (stroke) {
        ctx.stroke();
    }

}


function fullUrl(req) {
    return req.headers['x-forwarded-proto']+'://'+req.headers['host']+'/WeMake.png';
}

function hexToRgbA(hex){
    var c;
    if (!/^#/.test(hex)) {
        hex = '#'+hex;
    }
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length === 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+',1)';
    }
    throw new Error('Bad Hex');
}
