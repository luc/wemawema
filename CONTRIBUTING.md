# How to contribute?

## Add a translation

```
cd locales
cp en.json your_language.json
editor your_language.json
cd ..
```

Then edit `index.html` and add an item to the languages list (around line 60) like this:

```
<a class="dropdown-item" href="#" data-i18n="your_language">Your language</a>
```

The `data-i18n` attribute must match the name of your language file, without the `.json` extension.

In order to be able to be automatically chosen by the browser, you must name your file with an [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).

Then commit your changes and create a merge request.

## Modifying the javascript

You will need [`uglify-js`](https://www.npmjs.com/package/uglify-js) (`sudo npm install uglify-js -g`).

Modify `js/wemawema.js` then do `uglifyjs -c -m -o js/wema_wema.min.js js/wema_wema.js`.

Then commit your changes and create a merge request.

## Other contributions

Just commit your changes and create a merge request.
