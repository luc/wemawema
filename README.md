# WemaWema

![WemaWema logo](https://framagit.org/luc/wemawema/raw/master/img/favicon.png)

## What is it?

It's a "WE MAKE PORN" meme generator

## How to use it?

Go to an instance (official instance [here](https://luc.frama.io/wemawema)) and follow your heart.

## How to install it?

```
git clone https://framagit.org/luc/wemawema.git
```

and configure your web server to serve the `wemawema` directory

## What about the service?

WemaWema provides a service which generates an image, suitable for embedding, like this:

![WE MAKE IMAGES](https://wema.fiat-tux.fr/?p=IMAGES)

```
![WE MAKE IMAGES](https://wema.fiat-tux.fr/?p=IMAGES)
```

This service is able to answer to [Mattermost](https://mattermost.com) [slash commands](https://docs.mattermost.com/developer/slash-commands.html).
It answers the URL of an image containing the provided text, and Mattermost will make it viewable in the current channel.

### Service installation

Install the dependencies (for Debian):

```
sudo apt-get install libcairo2-dev libjpeg-dev libpango1.0-dev libgif-dev libpng-dev build-essential nodejs
```

Go to WemaWema service folder and install the packages:

```
cd wemawema/server
npm install
```

Copy the systemd service file, adapt it to your installation, enable and start it:

```
sudo cp wemawema.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable wemawema.service
systemctl start wemawema.service
```

The service will listen on the unix socket `/tmp/wemawema.sock`

Add this to your nginx configuration, it should work well:
```
    location / {
        proxy_set_header    Host $host;
        proxy_pass  http://unix:/tmp/wemawema.sock:;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
```

## License

AGPLv3. See the [LICENSE](LICENSE) file for details

The licenses of the fonts are located in the [fonts directories](https://framagit.org/luc/wemawema/tree/master/fonts).

The "Téléchat" background image is a tribute to the [Téléchat](https://fr.wikipedia.org/wiki/T%C3%A9l%C3%A9chat) tv show created by Roland Topor and Henri Xhonneux and is an adaptation of the copyrighted logo of the tv show.

## Author

[Luc Didry](https://fiat-tux.fr)

You can support the developer on [Liberapay](https://liberapay.com/sky/) or [Tipeee](https://www.tipeee.com/fiat-tux).

## Contributors

* Quentin Pagès, occitan translation

## Logo

Logo "Fez Hat", by [GDJ](https://openclipart.org/user-detail/GDJ), public domain.
